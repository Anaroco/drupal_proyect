<?php

namespace Drupal\backend_rocks\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Backend rocks routes.
 */
class BartController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => 'Multiplicate por cero!',
    ];
    $build['enlace'] = [
      '#type' => 'button',
      '#value' => \Drupal\Core\Url::fromRoute('<front>')->toString(),
    ];

    $build['plantilla'] = [
      '#theme' => 'time',
      '#timestamp' => time(),
    ];

    $build['template'] = [
      '#type' => 'inline_template',
      '#template' => "<p>{{ 'now'|date('d-m-Y') }}</p>",
    ];

    return $build;
  }

}
